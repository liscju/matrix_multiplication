#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "matrix.h"

int main(int argc, char **argv) {
	int i,j;
	int* matrix1, *matrix2, *matrix_result; 
	int version, size;

	if (argc != 3) {
		printf("Usage:\n %s version size\n", argv[0]);
		printf("Version can be one of: 0(for base), 1(for simd) 2(for blocking)\n");
		exit(0);
	}

	version = atoi(argv[1]);
	size = atoi(argv[2]);

	srand(time(NULL));

	matrix1 = initialize_matrix(size);
	fill_matrix_with_random_values(matrix1, size);	

	matrix2 = initialize_matrix(size);
	fill_matrix_with_random_values(matrix2, size);

	struct timeval time_start, time_end;

	gettimeofday(&time_start, NULL);

	if (version == 0) {
		matrix_result = 
			matrix_multiply_base(matrix1, matrix2, size);
	} else if (version == 1) {
		printf("Max number of threads - %d\n", omp_get_max_threads());
		matrix_result = 
			matrix_multiply_simd(matrix1, matrix2, size);
	} else if (version == 2) {
		printf("Max number of threads - %d\n", omp_get_max_threads());
		matrix_result = 
			matrix_multiply_manual_blocking(matrix1, matrix2, size);
	} else {
		printf("version number not supported");
	}

	gettimeofday(&time_end, NULL);

	int time = (time_end.tv_sec - time_start.tv_sec) * 1000 +
                   (time_end.tv_usec - time_start.tv_usec) / 1000;


	printf("Matrix multiplication of size %d calculated in time %d ms\n",
               size, time);
	/**
	  printf("matrix 1 =\n");
	  print_matrix(matrix1, size);

	  printf("matrix 2 =\n");
	  print_matrix(matrix2, size);

	  printf("matrix result =\n");
	  print_matrix(matrix_result, size);
	  */
	return 0;
}
