CC=gcc
CFLAGS=-O0

all: main.o

main.o: main.c matrix.o
	gcc -fopenmp $(CFLAGS) main.c -o main.o matrix.o

matrix.o: matrix.h matrix.c
	gcc -fopenmp $(CFLAGS) -c matrix.c

test: matrix.o
	gcc -fopenmp $(CFLAGS) test.c matrix.o -o test.o
	./test.o

clean:
	rm -f *.o
