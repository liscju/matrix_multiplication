#ifndef __MATRIX_H__
#define __MATRIX_H__

#include <time.h>
#include <stdlib.h>
#include <stdio.h>
#include <omp.h>

int* initialize_matrix(int size);
int matrix_get_index(int size, int row, int col);
void fill_matrix_with_random_values(int* matrix, int size);
void matrix_fill_zeros(int *matrix, int size);
int *matrix_multiply_base(int *matrix1, int *matrix2, int size);
int *matrix_multiply_simd(int *matrix1, int *matrix2, int size);
int *matrix_multiply_manual_blocking(int *matrix1, int *matrix2, int size);
void print_matrix(int *matrix, int size);

#endif // __MATRIX_H__
