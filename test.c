#include "matrix.h"

#include <assert.h>

void assert_matrix_equal(
	int *matrix1,
	int *matrix2,
	int size) {

	int i;
	for (i=0;i<size*size;i++) {
		if (matrix1[i] != matrix2[i]) {
			printf("For matrix of size %d changes in element %d", size, i);	
		}
		assert(matrix1[i] == matrix2[i]);
	}
}

void run_test(int size) {
	int *matrix1, *matrix2, 
	    *matrix_result_base,
            *matrix_result_simd,
            *matrix_result_manual_blocking; 

	matrix1 = initialize_matrix(size);

	fill_matrix_with_random_values(
		matrix1, size);	

	matrix2 = initialize_matrix(size);

	fill_matrix_with_random_values(
		matrix2, size);
	
	matrix_result_base = 
		matrix_multiply_base(
			matrix1, matrix2, size);

	matrix_result_simd = 
		matrix_multiply_simd(
			matrix1, matrix2, size);

	matrix_result_manual_blocking = 
		matrix_multiply_manual_blocking(
			matrix1, matrix2, size);

	assert_matrix_equal(
		matrix_result_base,
		matrix_result_simd,
		size);

	assert_matrix_equal(
		matrix_result_base,
		matrix_result_manual_blocking,
		size);

	assert_matrix_equal(
		matrix_result_simd,
		matrix_result_manual_blocking,
		size);
}

int main() {		
	int i, j;
	int matrix_test_sizes[] = {5, 10, 20, 40, 60, 80, 100};

	for (i=0;i < sizeof(matrix_test_sizes)/sizeof(int);i++) {
		for (j=0;j<5;j++) {
			run_test(matrix_test_sizes[i]);
			printf(
			   "Passed %d test of %d for matrixes of size %d\n",
				j, 5, matrix_test_sizes[i]);
		}
	}
	printf("All test passed!!\n");

	return 0;
}
