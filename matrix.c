#include "matrix.h"

int* initialize_matrix(int size) {	
	int i;
	int* matrix = malloc(size * size * sizeof(int));
	return matrix;
}

int matrix_get_index(int size, int row, int col) {
	return row * size + col;
}

void fill_matrix_with_random_values(int* matrix, int size) {
	int i, j;
	for (i=0;i < size;i++) {
		for (j=0;j < size;j++) {
			// modulo 20 to make later multiplication not overflow
			matrix[matrix_get_index(size, i, j)] = rand() % 20;
		}
	}
}

void matrix_fill_zeros(int *matrix, int size) {
	int i, j;
	for (i=0;i < size;i++) {
		for (j=0;j < size;j++) {
			// modulo 20 to make later multiplication not overflow
			matrix[matrix_get_index(size, i, j)] = 0;
		}
	}
}

int *matrix_multiply_base(int *matrix1, int *matrix2, int size) {

	int i;
	int *matrix_result = initialize_matrix(size);
	matrix_fill_zeros(matrix_result, size);

	#pragma omp parallel for private (i)
	for (i=0;i < size;i++) {
		int j,k;
		for (j=0;j < size;j++) {
			for (k=0;k < size;k++) {
				matrix_result[matrix_get_index(size, i, j)] += 
					matrix1[matrix_get_index(size, i, k)] *
					matrix2[matrix_get_index(size, k, j)];
			}
		}
	}
	return matrix_result;
}

int *matrix_multiply_simd(int *matrix1, int *matrix2, int size) {

	int i;
	int *matrix_result = initialize_matrix(size);
	matrix_fill_zeros(matrix_result, size);

	#pragma omp parallel for private (i)
	for (i=0;i < size;i++) {
		int j,k;
		for (k=0;k < size;k++) {
			#pragma omp simd
			for (j=0;j < size;j++) {
				matrix_result[matrix_get_index(size, i, j)] += 
					matrix1[matrix_get_index(size, i, k)] *
					matrix2[matrix_get_index(size, k, j)];
			}
		}
	}
	return matrix_result;
}

int minimum(int a, int b) {
	return a < b ? a : b;
}

int *matrix_multiply_manual_blocking(int *matrix1, int *matrix2, int size) {
	const int block_size = 64;

	int k1,j1;
	int *matrix_result = initialize_matrix(size);
	matrix_fill_zeros(matrix_result, size);

	for (k1 = 0; k1 < size; k1 += block_size) { 
#pragma omp parallel for private(j1) shared(k1)
		for (j1 = 0; j1 < size; j1 += block_size) {
			int i,k,j;
			for (i=0;i < size;i++) {
				for (k=k1;k < minimum(k1 + block_size, size);k++) {
#pragma omp simd
					for (j=j1;j < minimum(j1 + block_size, size);j++) {
						matrix_result[matrix_get_index(size, i, j)] += 
							matrix1[matrix_get_index(size, i, k)] *
							matrix2[matrix_get_index(size, k, j)];
					}
				}
			}
		}
	}
	return matrix_result;
}

void print_matrix(int *matrix, int size) {	
	int i,j;
	for (i=0; i < size; i++) {
		for (j=0; j < size; j++) {
			printf("%d ", matrix[matrix_get_index(size, i, j)]);
		}
		printf("\n");
	}
}
